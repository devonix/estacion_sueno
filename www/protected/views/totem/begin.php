<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<script>    
    var screen = 1;
    var num_screens = 2;
    
    function heightFormatter(value){
        return value.toFixed(2) + ' m';
    }
    function weightFormatter(value){
        return value.toFixed(0) + ' kg';
    }
    function ageFormatter(value){
        return value.toFixed(0) + ' años';
    }
    function heightChange(value){
        $('input[name="Profile[height]"]').val($("#Profile_height").data("slider").getValue().toFixed(2));
    }
    function previous(){
        screen--;
        showScreen(screen);
    }
    function next(){
        $('#Registration_submit').click();
    }
    function showScreen(id){
        $('.screen').removeClass('active');
        $('#screen'+id).addClass('active');
    }
    
    function onFormValidate(form, data, hasError){
        if (hasError){
            for (var s=1; s<screen+1; s++){
                var errors = $('#screen'+s).find('.errorMessage:visible');
                if (errors.length > 0){
                    showScreen(s);
                    console.log(errors);
                    return;
                }
            }
        }
        screen++;
        if (screen < num_screens+1){
            $('.screen').find('.errorMessage').hide();
            showScreen(screen);
        }else{
            document.getElementById('registration-form').submit();
        }    
    }
</script>

<div class="row span12 center">    
    <div class="span3" style="margin-top: 200px; text-align: center;">
        <?php echo CHtml::image(Yii::app()->baseUrl . '/images/ico_registration.png'); ?>
        <h1>Datos personales</h1>
        <h4>Complete el formulario</h4>
    </div>   
    <span class="span7 offset1" style="margin-top: 0px; text-align: center;">
        <?php if(Yii::app()->user->hasFlash('registration')): ?>
        <div class="success">
        <?php echo Yii::app()->user->getFlash('registration'); ?>
        </div>
        <?php else: ?>
        
        <div class="form">
        <?php $form=$this->beginWidget('UActiveForm', array(
            'id'=>'registration-form',
            'enableAjaxValidation'=>true,
            //'enableClientValidation'=>true,
            'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
            //'disableClientValidationAttributes'=>array('RegistrationForm_verifyCode'),
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'afterValidate'=>'js:onFormValidate',
                'validateOnChange'=>false,
            ),    
            'htmlOptions' => array('enctype'=>'multipart/form-data'),
        )); ?>
            
            <?php //echo $form->errorSummary(array($model,$profile)); ?>
            
            <div class="wizard">
                <div id="screen1" class="screen active" style="margin-top: 50px;">
                    
                    <!--<input type="hidden" name="Profile[company_id]" value="<?php echo isset($_COOKIE['hss_company'])?$_COOKIE['hss_company']:""; ?>">-->
                    <?php echo $form->hiddenField($profile, 'company_id', array('value'=>isset($_COOKIE['hss_company'])?$_COOKIE['hss_company']:"")); ?>
                    
                    <input type="hidden" name="totem" id="totem" value="1" />
        
                    <div class="row">
                    <?php 
                        echo $form->labelEx($profile, 'gender');
                        echo '<div class="form-inline form-inline-radios">';
                        echo $form->radioButtonList($profile, 'gender', array(
                                        '1'=>CHtml::image(Yii::app()->baseUrl . '/images/ico_male.png'), 
                                        '2'=>CHtml::image(Yii::app()->baseUrl . '/images/ico_female.png'),
                                    ), array('separator'=>''));                
                        echo '</div>';
                        echo $form->error($profile, 'gender');
                    ?>
                    <div class="touch-hint"></div>
                    </div>
                    
                    <div class="row">
                    <?php echo $form->labelEx($profile, 'height');
                          $this->widget('bootstrap.widgets.TbSlider', 
                                array( 
                                        'model'=>$profile,
                                        'attribute'=>'height',
                                        'options'=>array(
                                            'min'=>1.2,
                                            'max'=>2.2,
                                            'value'=>1.6,
                                            'step'=>0.01,
                                            'formater'=>'js:heightFormatter',
                                        ),
                                        'htmlOptions'=>array('value'=>'1.6'),
                                    )
                                ); 
                          echo $form->error($profile, 'height');
                    ?>
                    </div>
                                
                    <div class="row">
                    <?php echo $form->labelEx($profile, 'weight');
                          $this->widget('bootstrap.widgets.TbSlider', 
                                array( 
                                        'model'=>$profile,
                                        'attribute'=>'weight',
                                        'options'=>array(
                                            'min'=>40,
                                            'max'=>180,
                                            'value'=>70,
                                            'step'=>1,
                                            'formater'=>'js:weightFormatter',
                                        ),
                                        'htmlOptions'=>array('value'=>'70'),
                                    )
                                ); 
                          echo $form->error($profile, 'weight');
                    ?>
                    </div>
                    <div class="row">
                    <?php echo $form->labelEx($profile, 'age');
                          $this->widget('bootstrap.widgets.TbSlider', 
                                array( 
                                        'model'=>$profile,
                                        'attribute'=>'age',
                                        'options'=>array(
                                            'min'=>4,
                                            'max'=>99,
                                            'value'=>30,
                                            'step'=>1,
                                            'formater'=>'js:ageFormatter',
                                        ),
                                        'htmlOptions'=>array('value'=>'30'),
                                    )
                                ); 
                          echo $form->error($profile, 'age');
                    ?>
                    </div>
                    <div class="row">
                    <?php 
                        echo $form->labelEx($profile, 'marital_status');
                        echo '<div class="form-inline-narrow">';
                        echo $form->radioButtonList($profile, 'marital_status', array('1'=>'Soltero', '2'=>'Casado', '3'=>'Divorciado', '4'=>'Viudo'), array('separator'=>''));                
                        echo '</div>';
                        echo $form->error($profile, 'marital_status');
                    ?>
                    </div>
                                
                    
                    <button type="button" class="btn btn-large btn-green" onclick="next();">Continuar&nbsp;<i class="icon-chevron-right icon-white"></i></button>
                </div>
                <div id="screen2" class="screen" style="margin-top: 60px;">
                    
                    <div class="row">
                    <?php 
                        echo $form->labelEx($profile, 'occupation_id');
                        echo $form->dropDownList($profile, 'occupation_id', CHtml::listData(Occupation::model()->findAll(), 'id', 'name'), array('prompt'=>'Seleccione una opción'));
                        echo $form->error($profile, 'occupation_id');
                    ?>
                    </div>        
                    
                    <div class="row">
                    <?php 
                        echo $form->labelEx($profile, 'activity_level');
                        echo '<div class="form-inline form-inline-radios">';
                        echo $form->radioButtonList($profile, 'activity_level', array(
                                        '1'=>CHtml::image(Yii::app()->baseUrl . '/images/ico_activity_low.png', 'Sedentario', array('title'=>'Sedentario')), 
                                        '2'=>CHtml::image(Yii::app()->baseUrl . '/images/ico_activity_medium.png', 'Normal', array('title'=>'Normal')),
                                        '3'=>CHtml::image(Yii::app()->baseUrl . '/images/ico_activity_high.png', 'Muy activo', array('title'=>'Muy activo')),
                                    ), array('separator'=>''));                
                        echo '</div>';                
                        echo $form->error($profile, 'activity_level');
                    ?>
                    </div>
        
                    <div class="row form-inline-wide">
                    <?php
                        echo $form->labelEx($profile, 'working_days_sleep_hours');
                        echo '&nbsp;&nbsp;&nbsp;';
                        $this->widget('bootstrap.widgets.TbTimePicker', 
                                array( 
                                        'model'=>$profile,
                                        'attribute'=>'working_days_sleep_hours',
                                        'options' => array(
                                            'defaultTime' => '0:00',
                                            'noAppend' => true, // mandatory
                                            'disableFocus' => true, // mandatory
                                            'showMeridian' => false, // irrelevant
                                            'showInputs' => false,
                                            'disableFocus' => true
                                        ),
                                        'htmlOptions'=>array('style'=>'width: 40px;'),
                                    )
                                ); 
                        echo $form->error($profile, 'working_days_sleep_hours');
                    ?>
                    </div>
                    
                    <div class="row form-inline-wide">
                    <?php 
                        echo $form->labelEx($profile, 'working_days_sleep_hours_desired');
                        echo '&nbsp;&nbsp;&nbsp;';
                        $this->widget('bootstrap.widgets.TbTimePicker', 
                                array( 
                                        'model'=>$profile,
                                        'attribute'=>'working_days_sleep_hours_desired',
                                        'options' => array(
                                            'defaultTime' => '0:00',
                                            'noAppend' => true, // mandatory
                                            'disableFocus' => true, // mandatory
                                            'showMeridian' => false, // irrelevant
                                            'showInputs' => false,
                                            'disableFocus' => true
                                        ),
                                        'htmlOptions'=>array('style'=>'width: 40px;'),
                                    )
                                ); 
                        echo $form->error($profile, 'working_days_sleep_hours_desired');
                    ?>
                    </div>
                    
                    <div class="row">
                    <?php 
                        echo $form->labelEx($profile, 'working_days_sleep_quality');
                        echo '<div class="form-inline-narrow">';
                        echo $form->radioButtonList($profile, 'working_days_sleep_quality', array('1'=>'Muy mala', '2'=>'Mala', '3'=>'Buena', '4'=>'Muy buena'), array('separator'=>''));
                        echo '</div>';
                        echo $form->error($profile, 'working_days_sleep_quality');
                    ?>
                    </div>                    
                    
                    <button type="button" class="btn btn-large btn-green" onclick="next();">Continuar&nbsp;<i class="icon-chevron-right icon-white"></i></button>
                </div>
            </div>
            
            
        <?php /*$this->widget('bootstrap.widgets.TbButton', array( 
            'buttonType'=>'submit', 
            'type'=>'primary', 
            'size'=>'large',
            'label'=>UserModule::t('Register'), 
            'icon'=>'white ok',
        ));*/ ?>
        <button id="Registration_submit" type="submit" class="btn btn-green btn-large hidden">Siguiente</button>
        <?php //echo CHtml::link('Continuar', array("totem/begin"), array("class"=>"btn btn-green btn-large")); ?>            
        
        <?php $this->endWidget(); ?>
        </div><!-- form -->
        <?php endif; ?>
        
        
    </span>
</div>