<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
    UserModule::t("Login"),
);
?>

<div class="row span12 center">
    
<meta name="google-signin-clientid" content="762087741424-9h2mvev7e76u7o1064ke3ogmpqcriccq.apps.googleusercontent.com" />
<meta name="google-signin-scope" content="https://www.googleapis.com/auth/plus.login" />
<meta name="google-signin-requestvisibleactions" content="http://schemas.google.com/AddActivity" />
<meta name="google-signin-cookiepolicy" content="single_host_origin" />
    
<div class="pull-left span5">
    <h1>Soy un usuario nuevo</h1>
    <p>Complete el formulario de registro para comenzar a utilizar el sitio.</p>
    <br>
    <?php $this->widget('bootstrap.widgets.TbButton', array( 
        'buttonType'=>'link', 
        'type'=>'primary', 
        'label'=>'Crear cuenta', 
        'icon'=>'white plus',
        'url'=>array('/user/registration'),
    )); ?> 
    <br><br>
    
    <h1>Ingresar con Google</h1>
    <p>Utilice el botón para acceder con su cuenta de Google.</p>
    <br>
    <script type="text/javascript">
     (function() {
       var po = document.createElement('script');
       po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js?onload=render';
       var s = document.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(po, s);
     })();
    
       /* Executed when the APIs finish loading */
     function render() {
    
       // Additional params including the callback, the rest of the params will
       // come from the page-level configuration.
       var additionalParams = {
         'callback': signinCallback
       };
    
       // Attach a click listener to a button to trigger the flow.
       var signinButton = document.getElementById('signinButton');
       signinButton.addEventListener('click', function() {
         gapi.auth.signIn(additionalParams); // Will use page level configuration
       });
     }
     
     function signinCallback(authResult) {
       if (authResult['status']['signed_in']) {
         // Update the app to reflect a signed in user
         // Hide the sign-in button now that the user is authorized, for example:
         document.getElementById('signinButton').setAttribute('style', 'display: none');
         
         gapi.client.load('plus','v1', function(){
           var request = gapi.client.plus.people.get({
             'userId': 'me'
           });
           request.execute(function(resp) {
               console.log('ID: ' + resp.id);
               console.log('Display Name: ' + resp.displayName);
               console.log('Image URL: ' + resp.image.url);
               console.log('Profile URL: ' + resp.url);
               window.location.href='?r=user/registration&Profile[provider]=Google&Profile[provider_id]='+resp.id+'&Profile[name]='+resp.displayName;
           });
         });
       } else {
         // Update the app to reflect a signed out user
         // Possible error values:
         //   "user_signed_out" - User is signed-out
         //   "access_denied" - User denied access to your app
         //   "immediate_failed" - Could not automatically log in the user
         console.log('Sign-in state: ' + authResult['error']);
       }
     }
    
    </script>
    <button class="btn btn-primary" id="signinButton">Ingresar con Google</button>
    
    <!-- Place this asynchronous JavaScript just before your </body> tag -->
    <script type="text/javascript">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
    </script>        
</div>
<div class="pull-left span6">
    
    <h1>Ya tengo una cuenta</h1>
    
    <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
    
    <div class="success">
        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
    </div>
    
    <?php endif; ?>
    
    <p><?php echo UserModule::t("Please fill out the following form with your login credentials:"); ?></p>
    
    <div class="form">
    <?php echo CHtml::beginForm(); ?>
    
        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
        
        <?php echo CHtml::errorSummary($model); ?>
        
        <div class="span4">
        <div class="row">
            <?php echo CHtml::activeLabelEx($model,'username'); ?>
            <?php echo CHtml::activeTextField($model,'username') ?>
        </div>
        
        <div class="row">
            <?php echo CHtml::activeLabelEx($model,'password'); ?>
            <?php echo CHtml::activePasswordField($model,'password') ?>
        </div>
        
        <div class="row">
            <p class="hint">
            <?php /*echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> | <?php */ echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
            </p>
        </div>
        
        <div class="row rememberMe">
            <?php echo CHtml::activeCheckBox($model,'rememberMe',array('class'=>'pull-left')); ?>
            <?php echo CHtml::activeLabelEx($model,'rememberMe',array('class'=>'pull-left', 'style'=>'margin-left:5px;')); ?>
        </div>
        <br>
        <div class="row submit">
            <?php $this->widget('bootstrap.widgets.TbButton', array( 
                'buttonType'=>'submit', 
                'type'=>'primary', 
                'label'=>UserModule::t('Login'), 
                'icon'=>'white ok',
            )); ?>   
        </div>
        </div>
        
    <?php echo CHtml::endForm(); ?>
    </div><!-- form -->
    
    
    <?php
    $form = new CForm(array(
        'elements'=>array(
            'username'=>array(
                'type'=>'text',
                'maxlength'=>32,
            ),
            'password'=>array(
                'type'=>'password',
                'maxlength'=>32,
            ),
            'rememberMe'=>array(
                'type'=>'checkbox',
            )
        ),
    
        'buttons'=>array(
            'login'=>array(
                'type'=>'submit',
                'label'=>'Login',
            ),
        ),
    ), $model);
?>
</div>
</div>