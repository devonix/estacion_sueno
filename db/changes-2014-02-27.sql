ALTER TABLE `user_test` CHANGE `working_days_sleep_hours` `working_days_sleep_hours` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `working_days_sleep_hours_desired` `working_days_sleep_hours_desired` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `weekend_sleep_hours` `weekend_sleep_hours` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `weekend_sleep_hours_desired` `weekend_sleep_hours_desired` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `working_hours` `working_hours` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `exercise_hours` `exercise_hours` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `recreation_hours` `recreation_hours` TIME DEFAULT '00:00:00' NOT NULL, 
CHANGE `travel_hours` `travel_hours` TIME DEFAULT '00:00:00' NOT NULL; 