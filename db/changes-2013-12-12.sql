CREATE TABLE `occupation`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `occupation` (`id`, `name`) VALUES('1','Empresario');
INSERT INTO `occupation` (`id`, `name`) VALUES('2','Profesional Autónomo');
INSERT INTO `occupation` (`id`, `name`) VALUES('3','Comerciante');
INSERT INTO `occupation` (`id`, `name`) VALUES('4','Dirección de Empresas');
INSERT INTO `occupation` (`id`, `name`) VALUES('5','Gerencia');
INSERT INTO `occupation` (`id`, `name`) VALUES('6','Jefatura');
INSERT INTO `occupation` (`id`, `name`) VALUES('7','Supervisión');
INSERT INTO `occupation` (`id`, `name`) VALUES('8','Empleado');
INSERT INTO `occupation` (`id`, `name`) VALUES('9','En búsqueda laboral');
INSERT INTO `occupation` (`id`, `name`) VALUES('10','Otros');

CREATE TABLE `occupation_area`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `occupation_area` (`id`, `name`) VALUES('1','Administración pública');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('2','Agricultura, ganadería, pesca');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('3','Alimentos y bebidas');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('4','Banca y servicios financieros');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('5','Comunicación, marketing y publicidad');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('6','Construcción y mercado inmobiliario');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('7','Consultoría');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('8','Consumo masivo');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('9','Cosmética y limpieza');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('10','Defensa y seguridad');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('11','Electrónica y electrodomésticos');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('12','Energía, electricidad, gas y petróleo');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('13','Enseñanza');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('14','Estudios jurídicos');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('15','Eventos y exposiciones');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('16','Indumentaria');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('17','Industria automotriz');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('18','Laboratorios');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('19','Medios de comunicación');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('20','Mercado de capitales');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('21','Minería');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('22','ONG, servicios comunitarios y sociales');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('23','Química y petroquímica');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('24','Retail');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('25','Seguros');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('26','Servicios a empresas');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('27','Servicios de salud');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('28','Siderurgia y metalurgia');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('29','Tabacaleras');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('30','Tecnología');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('31','Telecomunicaciones');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('32','Transporte');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('33','Turismo, hotelería, restaurantes');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('34','Universidades y educación');
INSERT INTO `occupation_area` (`id`, `name`) VALUES('35','Otras actividades');

ALTER TABLE `profile` DROP COLUMN `occupation`, DROP COLUMN `occupation_area`;

ALTER TABLE `profile` ADD COLUMN occupation_id INT NOT NULL;
ALTER TABLE `profile` ADD COLUMN occupation_area_id INT NOT NULL;

/*[03:50:42 p.m.][33 ms]*/ DELETE FROM `profile_field` WHERE `varname` = 'occupation'; 
/*[03:50:42 p.m.][2 ms]*/ DELETE FROM `profile_field` WHERE `varname` = 'occupation_area'; 

/*[03:52:08 p.m.][45 ms]*/ INSERT INTO `profile_field` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) 
VALUES (NULL, 'occupation_id', 'Profesión', 'INTEGER', '10', '0', '1', '', '', '', '', '0', 'UWrelBelongsTo', '{\"modelName\":\"Occupation\",\"optionName\":\"name\",\"relationName\":\"occupation\"}', '61', '1'); 

/*[03:52:35 p.m.][5 ms]*/ INSERT INTO `profile_field` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) 
VALUES (NULL, 'occupation_area_id', 'Área de actividad', 'INTEGER', '10', '0', '1', '', '', '', '', '0', 'UWrelBelongsTo', '{\"modelName\":\"OccupationArea\",\"optionName\":\"name\",\"relationName\":\"occupation_area\"}', '71', '1'); 

UPDATE test SET name='Escala de somnolencia diurna' WHERE  id=3;

/*[04:21:39 p.m.][29 ms]*/ UPDATE `profile_field` SET `title` = 'Horas de sueño habituales en días laborales' WHERE `id` = '20'; 
/*[04:21:52 p.m.][10 ms]*/ UPDATE `profile_field` SET `title` = 'Horas de sueño habituales en fines de semana' WHERE `id` = '23';