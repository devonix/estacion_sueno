-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.27 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para estacion_sueno
CREATE DATABASE IF NOT EXISTS `estacion_sueno` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `estacion_sueno`;


-- Volcando estructura para tabla estacion_sueno.authassignment
DROP TABLE IF EXISTS `authassignment`;
CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.authassignment: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `authassignment` DISABLE KEYS */;
REPLACE INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
	('Admin', '1', NULL, 'N;');
/*!40000 ALTER TABLE `authassignment` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.authitem
DROP TABLE IF EXISTS `authitem`;
CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.authitem: ~28 rows (aproximadamente)
/*!40000 ALTER TABLE `authitem` DISABLE KEYS */;
REPLACE INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
	('Admin', 2, NULL, NULL, 'N;'),
	('Authenticated', 2, NULL, NULL, 'N;'),
	('Guest', 2, NULL, NULL, 'N;'),
	('Site.Index', 0, NULL, NULL, 'N;'),
	('Table.*', 1, NULL, NULL, 'N;'),
	('Table.Admin', 0, NULL, NULL, 'N;'),
	('Table.Create', 0, NULL, NULL, 'N;'),
	('Table.Delete', 0, NULL, NULL, 'N;'),
	('Table.Index', 0, NULL, NULL, 'N;'),
	('Table.Update', 0, NULL, NULL, 'N;'),
	('Table.View', 0, NULL, NULL, 'N;'),
	('User.Activation.*', 1, NULL, NULL, 'N;'),
	('User.Activation.Activation', 0, NULL, NULL, 'N;'),
	('User.Default.*', 1, NULL, NULL, 'N;'),
	('User.Default.Index', 0, NULL, NULL, 'N;'),
	('User.Login.*', 1, NULL, NULL, 'N;'),
	('User.Login.Login', 0, NULL, NULL, 'N;'),
	('User.Logout.*', 1, NULL, NULL, 'N;'),
	('User.Logout.Logout', 0, NULL, NULL, 'N;'),
	('User.Profile.*', 1, NULL, NULL, 'N;'),
	('User.Profile.Changepassword', 0, NULL, NULL, 'N;'),
	('User.Profile.Edit', 0, NULL, NULL, 'N;'),
	('User.Profile.Profile', 0, NULL, NULL, 'N;'),
	('User.Recovery.*', 1, NULL, NULL, 'N;'),
	('User.Recovery.Recovery', 0, NULL, NULL, 'N;'),
	('User.User.*', 1, NULL, NULL, 'N;'),
	('User.User.Index', 0, NULL, NULL, 'N;'),
	('User.User.View', 0, NULL, NULL, 'N;');
/*!40000 ALTER TABLE `authitem` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.authitemchild
DROP TABLE IF EXISTS `authitemchild`;
CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.authitemchild: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `authitemchild` DISABLE KEYS */;
REPLACE INTO `authitemchild` (`parent`, `child`) VALUES
	('Authenticated', 'Site.Index'),
	('Authenticated', 'Table.*'),
	('Authenticated', 'Table.Create'),
	('Authenticated', 'Table.Index'),
	('Authenticated', 'Table.Update'),
	('Authenticated', 'Table.View'),
	('Authenticated', 'User.Activation.*'),
	('Authenticated', 'User.Activation.Activation'),
	('Authenticated', 'User.Default.*'),
	('Authenticated', 'User.Default.Index'),
	('Authenticated', 'User.Login.Login'),
	('Authenticated', 'User.Logout.*'),
	('Authenticated', 'User.Logout.Logout'),
	('Authenticated', 'User.Profile.*'),
	('Authenticated', 'User.Profile.Changepassword'),
	('Authenticated', 'User.Profile.Edit'),
	('Authenticated', 'User.Profile.Profile'),
	('Authenticated', 'User.Recovery.Recovery');
/*!40000 ALTER TABLE `authitemchild` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.profile
DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender` int(10) NOT NULL DEFAULT '0',
  `height` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `age` int(10) NOT NULL DEFAULT '0',
  `marital_status` int(10) NOT NULL DEFAULT '0',
  `occupation` varchar(255) NOT NULL DEFAULT '',
  `occupation_area` varchar(255) NOT NULL DEFAULT '',
  `activity_level` int(10) NOT NULL DEFAULT '0',
  `working_days_sleep_hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `working_days_sleep_hours_desired` decimal(10,2) NOT NULL DEFAULT '0.00',
  `working_days_sleep_quality` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weekend_sleep_hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weekend_sleep_hours_desired` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weekend_sleep_quality` int(10) NOT NULL DEFAULT '0',
  `working_hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `exercise_hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `recreation_hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `travel_hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla estacion_sueno.profile: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
REPLACE INTO `profile` (`user_id`, `name`, `gender`, `height`, `weight`, `age`, `marital_status`, `occupation`, `occupation_area`, `activity_level`, `working_days_sleep_hours`, `working_days_sleep_hours_desired`, `working_days_sleep_quality`, `weekend_sleep_hours`, `weekend_sleep_hours_desired`, `weekend_sleep_quality`, `working_hours`, `exercise_hours`, `recreation_hours`, `travel_hours`) VALUES
	(1, 'Administrador', 0, 1.80, 60.00, 0, 0, '', '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00),
	(6, 'Demo', 0, 1.80, 60.00, 0, 0, '', '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00),
	(7, 'Maxi', 1, 1.50, 52.00, 42, 3, 'Ninguna', 'Ninguna', 3, 1.00, 2.00, 1.00, 8.00, 22.00, 1, 0.00, 0.00, 24.00, 0.00),
	(8, 'Maxi2', 1, 1.80, 60.00, 0, 1, '1', '1', 1, 1.00, 1.00, 1.00, 1.00, 1.00, 1, 1.00, 1.00, 1.00, 1.00),
	(9, 'Nombre', 1, 1.80, 60.00, 0, 4, 'profesion', 'area', 3, 3.00, 3.00, 2.00, 2.00, 2.00, 0, 0.00, 0.00, 0.00, 0.00),
	(10, 'test3', 2, 1.68, 68.00, 61, 3, 'profesion', 'area', 2, 0.00, 0.00, 3.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.profile_field
DROP TABLE IF EXISTS `profile_field`;
CREATE TABLE IF NOT EXISTS `profile_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla estacion_sueno.profile_field: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `profile_field` DISABLE KEYS */;
REPLACE INTO `profile_field` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
	(5, 'name', 'Name', 'VARCHAR', 255, 0, 1, '', '', '', '', '', '', '', 0, 1),
	(12, 'gender', 'Sexo', 'INTEGER', 10, 0, 1, '', '1==Masculino;2==Femenino', '', '', '0', '', '', 10, 1),
	(13, 'height', 'Altura', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 20, 1),
	(14, 'weight', 'Peso', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 30, 1),
	(15, 'age', 'Edad', 'INTEGER', 10, 0, 1, '', '', '', '', '0', '', '', 40, 1),
	(16, 'marital_status', 'Estado civil', 'INTEGER', 10, 0, 1, '', '1==Soltero;2==Casado;3==Divorciado;4==Viudo', '', '', '0', '', '', 50, 1),
	(17, 'occupation', 'Profesión', 'VARCHAR', 255, 0, 1, '', '', '', '', '', '', '', 60, 1),
	(18, 'occupation_area', 'Área de actividad laboral', 'VARCHAR', 255, 0, 1, '', '', '', '', '', '', '', 70, 1),
	(19, 'activity_level', 'Nivel de actividad', 'INTEGER', 10, 0, 1, '', '1==Sedentario;2==Activo;3==Muy activo', '', '', '0', '', '', 75, 1),
	(20, 'working_days_sleep_hours', 'Horas de sueño en días laborales', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 80, 1),
	(21, 'working_days_sleep_hours_desired', 'Horas de sueño deseadas en días laborales', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 80, 1),
	(22, 'working_days_sleep_quality', 'Calidad de sueño en días laborales', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 80, 1),
	(23, 'weekend_sleep_hours', 'Horas de sueño en fines de semana', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 110, 1),
	(24, 'weekend_sleep_hours_desired', 'Horas de sueño deseadas por noche en fines de semana', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 120, 1),
	(25, 'weekend_sleep_quality', 'Calidad de sueño en fines de semana', 'INTEGER', 10, 0, 1, '', '', '', '', '0', '', '', 130, 1),
	(26, 'working_hours', 'Horas de trabajo', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 140, 1),
	(27, 'exercise_hours', 'Horas de actividad física', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 150, 1),
	(28, 'recreation_hours', 'Horas de actividad recreativa', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 160, 1),
	(29, 'travel_hours', 'Horas de viaje', 'DECIMAL', 10, 0, 1, '', '', '', '', '0', '', '', 170, 1);
/*!40000 ALTER TABLE `profile_field` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.question
DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL,
  `number` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  `question_type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.question: ~40 rows (aproximadamente)
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
REPLACE INTO `question` (`id`, `test_id`, `number`, `text`, `question_type`) VALUES
	(1, 3, 1, 'Sentado leyendo', 1),
	(2, 3, 2, 'Viendo televisión', 1),
	(3, 3, 3, 'Sentado, inactivo en un lugar público', 1),
	(4, 3, 4, 'Como pasajero en un coche una hora seguida', 1),
	(5, 3, 5, 'Descansando recostado por la tarde cuando las circunsatancias lo permiten', 1),
	(6, 3, 6, 'Sentado charlando con alguien', 1),
	(7, 3, 7, 'Sentado tranquilo, después de un almuerzo sin alcohol', 1),
	(8, 3, 8, 'En un coche, al pararse unos minutos en el tráfico', 1),
	(9, 1, 1, 'en su casa, ¿a qué hora se acuesta normalmente por la noche?', 2),
	(10, 1, 2, '¿Cúanto tiempo demora en quedarse dormido en promedio?', 2),
	(11, 1, 3, '¿A qué hora se levanta habitualmente por la mañana?', 2),
	(12, 1, 4, '¿Cuántas horas duerme habitualmente cada noche?', 2),
	(13, 1, 5, 'No poder quedarse dormido en la primera media hora', 3),
	(14, 1, 6, 'Despertarse durante la noche o de madrugada', 3),
	(15, 1, 7, 'Tener que levantarse para ir al baño', 3),
	(16, 1, 8, 'No poder respirar bien', 3),
	(17, 1, 9, 'Toser o roncar ruidosamente', 3),
	(18, 1, 10, 'Sentir  frio', 3),
	(19, 1, 11, 'Sentir calor', 3),
	(20, 1, 12, 'Tener sueños feos o pesadillas', 3),
	(21, 1, 13, 'Tener dolores', 3),
	(22, 1, 14, 'Otras razones', 3),
	(23, 1, 15, 'Durante el mes pasado... ¿cuántas veces ha tomado medicinas (recetadas por el medico o por su cuenta) para dormir?', 3),
	(24, 1, 16, 'Durante el mes pasado... ¿cuántas veces ha tenido problemas para permanecer despierto mientras conducía, comía, trabajaba, estudiaba o desarrollaba alguna otra actividad social?', 3),
	(25, 1, 17, 'Durante el último mes, ¿qué tanto problema le ha traído a usted su estado de ánimo para realizar actividades como conducir, comer, trabajar, estudiar o alguna otra actividad social?', 4),
	(26, 1, 18, 'Durante el último mes, ¿cómo calificaría en conjunto la calidad de su sueño?', 5),
	(27, 2, 1, 'Ronquido fuerte', 6),
	(28, 2, 2, '¿Siente las piernas inquietas, como si saltaran, o que se sacuden? (En cualquier momento del día)?', 6),
	(29, 2, 3, 'Dificultad para conciliar el sueño', 6),
	(30, 2, 4, 'Despertares frecuentes', 6),
	(31, 2, 5, '¿Tiene dificultad para respirar? ¿Respiración entrecortada? ¿Respiración ruidosa? (En cualquier momento del día)', 6),
	(32, 2, 6, '¿Se siente adormecido en el trabajo? (mientras conduce)', 6),
	(33, 2, 7, '¿Con frecuencia da vueltas o se sacude en la cama?', 6),
	(34, 2, 8, '¿En algún momento siente que se queda sin aire? (en cualquier momento del día)', 6),
	(35, 2, 9, '¿Tiene excesiva somnolencia (le da mucho sueño) durante el día?', 6),
	(36, 2, 10, '¿Tiene dolores de cabeza por la mañana?', 6),
	(37, 2, 11, '¿Se sintió adormecido al conducir? (Fuera de su trabajo)', 6),
	(38, 2, 12, 'Se siente paralizado, incapaz de moverse por períodos cortos al dormirse o al despertar', 6),
	(39, 2, 13, '¿Tiene sueños vívidos al quedarse dormido o en el momento de despertarse?', 6),
	(40, 2, 14, 'Ronquido (Cualquiera; fuerte o débil)', 6);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.question_type
DROP TABLE IF EXISTS `question_type`;
CREATE TABLE IF NOT EXISTS `question_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.question_type: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
REPLACE INTO `question_type` (`id`, `name`) VALUES
	(1, 'Epworth'),
	(2, 'Pittsburgh - 1'),
	(3, 'Pittsburgh - 2'),
	(4, 'Pittsburgh - 3'),
	(5, 'Pittsburgh - 4'),
	(6, 'MAP');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.question_type_option
DROP TABLE IF EXISTS `question_type_option`;
CREATE TABLE IF NOT EXISTS `question_type_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_type_id` int(10) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question_type_id` (`question_type_id`),
  CONSTRAINT `question_type_option_ibfk_1` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.question_type_option: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `question_type_option` DISABLE KEYS */;
REPLACE INTO `question_type_option` (`id`, `question_type_id`, `value`, `text`) VALUES
	(1, 1, 0, 'Ninguna vez en el último mes'),
	(2, 1, 1, 'Menos de una vez por semana'),
	(3, 1, 2, 'Una o dos veces a la semana'),
	(4, 1, 3, 'Tres o más veces a la semana'),
	(5, 2, 0, 'Escriba la hora habitual en la que se acuesta hs/min'),
	(6, 3, 0, 'Ninguna vez en el último mes'),
	(7, 3, 1, 'Menos de una vez a la semana'),
	(8, 3, 2, 'Una o dos veces a la semana'),
	(9, 3, 3, 'Tres o más veces a la semana'),
	(10, 4, 0, 'Ningún problema'),
	(11, 4, 1, 'Poco problema'),
	(12, 4, 2, 'Moderado problema'),
	(13, 4, 3, 'Mucho problema'),
	(14, 5, 0, 'Muy buena'),
	(15, 5, 1, 'Bastante buena'),
	(16, 5, 2, 'Bastante mala'),
	(17, 5, 3, 'Muy mala'),
	(18, 6, 0, 'Nunca'),
	(19, 6, 1, 'Casi nunca'),
	(20, 6, 2, 'A veces'),
	(21, 6, 3, 'Con frecuencia'),
	(22, 6, 4, 'Siempre'),
	(23, 6, 5, 'No sabe');
/*!40000 ALTER TABLE `question_type_option` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.rights
DROP TABLE IF EXISTS `rights`;
CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`),
  CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.rights: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `rights` DISABLE KEYS */;
/*!40000 ALTER TABLE `rights` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.tbl_migration
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.tbl_migration: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
REPLACE INTO `tbl_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1357784830),
	('m110805_153437_installYiiUser', 1357784843),
	('m110810_162301_userTimestampFix', 1357784843);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.test
DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_reaction_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.test: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
REPLACE INTO `test` (`id`, `name`, `is_reaction_test`) VALUES
	(1, 'Pittsburgh', 0),
	(2, 'MAP', 0),
	(3, 'Epworth', 0),
	(4, 'Tiempo de reacción', 1);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla estacion_sueno.user: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'info@pixelmachine.com.ar', 'ebe83c88cff438a889d3a833d6b2d9e5', 1, 1, '2013-01-09 23:27:23', '2013-07-16 23:21:04'),
	(6, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@pixelmachine.com.ar', '2957aefeba7be834458f490d84577314', 1, 1, '2013-05-27 16:41:45', '2013-07-25 03:32:22'),
	(7, 'maxi', 'd6af6111b120b3b6d0edad7d1d3fc692', 'mnusspaumer@pixelmachine.com.ar', '49dbaa1666aea3feaf46980b0e4b3222', 0, 1, '2013-06-12 15:40:40', '2013-06-12 20:46:35'),
	(8, 'maxi2', 'c92d56303b128ae61e1c0af1af1cd7fd', 'maxi@pixelmachine.com.ar', '57d3f10c36016da5be411c980bb875ca', 0, 1, '2013-06-12 15:51:28', '2013-06-17 19:48:21'),
	(9, 'usuario_test', '81dc9bdb52d04dc20036dbd8313ed055', 'test@devonix.net', '85fa6d944d2bf8733e4245b50ccd83cf', 0, 1, '2013-07-04 17:42:45', '0000-00-00 00:00:00'),
	(10, 'test3', '81dc9bdb52d04dc20036dbd8313ed055', 'test3@devonix.net', 'b994bebd65eb276ff116b95414ca8f4a', 0, 1, '2013-07-04 17:55:12', '2013-07-25 01:17:14');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.user_reaction_test
DROP TABLE IF EXISTS `user_reaction_test`;
CREATE TABLE IF NOT EXISTS `user_reaction_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_test_id` int(10) unsigned NOT NULL,
  `sleep_hours` int(10) unsigned NOT NULL,
  `awake_hours` int(10) unsigned NOT NULL,
  `time_now` decimal(10,4) unsigned NOT NULL,
  `alert_level` int(10) unsigned NOT NULL,
  `invalid_count` int(10) unsigned NOT NULL,
  `missed_count` int(10) unsigned NOT NULL,
  `crash_count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_test_id` (`user_test_id`),
  CONSTRAINT `user_reaction_test_ibfk_1` FOREIGN KEY (`user_test_id`) REFERENCES `user_test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.user_reaction_test: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `user_reaction_test` DISABLE KEYS */;
REPLACE INTO `user_reaction_test` (`id`, `user_test_id`, `sleep_hours`, `awake_hours`, `time_now`, `alert_level`, `invalid_count`, `missed_count`, `crash_count`) VALUES
	(4, 58, 1, 1, 10.0000, 1, 0, 0, 0),
	(5, 59, 1, 1, 10.0000, 1, 0, 0, 0),
	(6, 60, 1, 1, 10.0000, 1, 0, 0, 0),
	(7, 61, 1, 1, 10.0000, 1, 0, 0, 0),
	(8, 62, 1, 1, 10.0000, 5, 0, 0, 0),
	(9, 63, 8, 3, 10.5000, 8, 0, 0, 0),
	(10, 64, 8, 6, 10.7500, 8, 0, 0, 0),
	(11, 65, 1, 1, 10.0167, 5, 0, 1, 3),
	(12, 69, 1, 1, 10.0167, 5, 14, 0, 4),
	(13, 70, 1, 1, 10.0167, 5, 4, 4, 1);
/*!40000 ALTER TABLE `user_reaction_test` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.user_reaction_test_measurement
DROP TABLE IF EXISTS `user_reaction_test_measurement`;
CREATE TABLE IF NOT EXISTS `user_reaction_test_measurement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_reaction_test_id` int(10) unsigned NOT NULL,
  `value` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_reaction_test_id` (`user_reaction_test_id`),
  CONSTRAINT `user_reaction_test_measurement_ibfk_3` FOREIGN KEY (`user_reaction_test_id`) REFERENCES `user_reaction_test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.user_reaction_test_measurement: ~55 rows (aproximadamente)
/*!40000 ALTER TABLE `user_reaction_test_measurement` DISABLE KEYS */;
REPLACE INTO `user_reaction_test_measurement` (`id`, `user_reaction_test_id`, `value`) VALUES
	(1, 4, 453.00),
	(2, 4, 533.00),
	(3, 5, 453.00),
	(4, 5, 533.00),
	(5, 6, 453.00),
	(6, 6, 533.00),
	(7, 7, 453.00),
	(8, 7, 533.00),
	(9, 8, 98.00),
	(10, 8, 475.00),
	(11, 9, 430.00),
	(12, 9, 413.00),
	(13, 10, 454.00),
	(14, 10, 498.00),
	(15, 10, 501.00),
	(16, 10, 476.00),
	(17, 10, 619.00),
	(18, 10, 395.00),
	(19, 10, 674.00),
	(20, 10, 423.00),
	(21, 10, 497.00),
	(22, 10, 456.00),
	(23, 10, 592.00),
	(24, 10, 493.00),
	(25, 10, 551.00),
	(26, 10, 482.00),
	(27, 10, 678.00),
	(28, 10, 635.00),
	(29, 10, 690.00),
	(30, 10, 504.00),
	(31, 10, 1521.00),
	(32, 10, -1.00),
	(33, 10, 513.00),
	(34, 10, 585.00),
	(35, 10, 856.00),
	(36, 10, 459.00),
	(37, 10, 578.00),
	(38, 10, 472.00),
	(39, 10, 496.00),
	(40, 10, 525.00),
	(41, 10, 525.00),
	(42, 10, 546.00),
	(43, 11, 464.00),
	(44, 11, 892.00),
	(45, 11, 916.00),
	(46, 11, 463.00),
	(47, 11, 1024.00),
	(48, 12, 2204.00),
	(49, 12, 606.00),
	(50, 12, 806.00),
	(51, 12, 555.00),
	(52, 12, 313.00),
	(53, 12, 339.00),
	(54, 13, 4323.00),
	(55, 13, 372.00);
/*!40000 ALTER TABLE `user_reaction_test_measurement` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.user_test
DROP TABLE IF EXISTS `user_test`;
CREATE TABLE IF NOT EXISTS `user_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `test_id` int(10) unsigned NOT NULL,
  `date_taken` datetime NOT NULL,
  `score` decimal(10,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `user_test_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_test_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.user_test: ~17 rows (aproximadamente)
/*!40000 ALTER TABLE `user_test` DISABLE KEYS */;
REPLACE INTO `user_test` (`id`, `user_id`, `test_id`, `date_taken`, `score`) VALUES
	(30, 10, 3, '2013-07-11 17:33:47', 0.0000),
	(31, 10, 3, '2013-07-12 14:50:48', 0.0000),
	(32, 10, 3, '2013-07-16 12:20:18', 24.0000),
	(45, 10, 2, '2013-07-23 16:05:11', 0.1602),
	(46, 10, 2, '2013-07-23 16:08:03', 0.3995),
	(47, 10, 2, '2013-07-23 16:10:23', 0.3995),
	(48, 6, 2, '2013-07-24 19:49:05', 0.0349),
	(58, 10, 4, '2013-07-29 18:02:35', 10.0000),
	(59, 10, 4, '2013-07-29 18:02:59', 10.0000),
	(60, 10, 4, '2013-07-29 18:03:25', 10.0000),
	(61, 10, 4, '2013-07-29 18:03:40', 10.0000),
	(62, 10, 4, '2013-07-29 18:28:14', 10.0000),
	(63, 10, 4, '2013-07-30 13:09:24', 10.0000),
	(64, 10, 4, '2013-07-30 13:13:27', 10.0000),
	(65, 10, 4, '2013-07-30 15:17:06', 751.8000),
	(69, 10, 4, '2013-07-30 15:32:19', 803.8333),
	(70, 10, 4, '2013-07-30 16:12:38', 2347.5000);
/*!40000 ALTER TABLE `user_test` ENABLE KEYS */;


-- Volcando estructura para tabla estacion_sueno.user_test_answer
DROP TABLE IF EXISTS `user_test_answer`;
CREATE TABLE IF NOT EXISTS `user_test_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_test_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `question_type_option_id` int(10) unsigned DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_test_id` (`user_test_id`),
  KEY `question_id` (`question_id`),
  KEY `question_type_option_id` (`question_type_option_id`),
  CONSTRAINT `user_test_answer_ibfk_1` FOREIGN KEY (`user_test_id`) REFERENCES `user_test` (`id`),
  CONSTRAINT `user_test_answer_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `user_test_answer_ibfk_3` FOREIGN KEY (`question_type_option_id`) REFERENCES `question_type_option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla estacion_sueno.user_test_answer: ~80 rows (aproximadamente)
/*!40000 ALTER TABLE `user_test_answer` DISABLE KEYS */;
REPLACE INTO `user_test_answer` (`id`, `user_test_id`, `question_id`, `question_type_option_id`, `value`) VALUES
	(16, 30, 1, 1, NULL),
	(17, 30, 2, 1, NULL),
	(18, 30, 3, 1, NULL),
	(19, 30, 4, 1, NULL),
	(20, 30, 5, 2, NULL),
	(21, 30, 6, 2, NULL),
	(22, 30, 7, 2, NULL),
	(23, 30, 8, 2, NULL),
	(24, 31, 1, 3, NULL),
	(25, 31, 2, 3, NULL),
	(26, 31, 3, 3, NULL),
	(27, 31, 4, 4, NULL),
	(28, 31, 5, 4, NULL),
	(29, 31, 6, 3, NULL),
	(30, 31, 7, 4, NULL),
	(31, 31, 8, 3, NULL),
	(32, 32, 1, 4, NULL),
	(33, 32, 2, 4, NULL),
	(34, 32, 3, 4, NULL),
	(35, 32, 4, 4, NULL),
	(36, 32, 5, 4, NULL),
	(37, 32, 6, 4, NULL),
	(38, 32, 7, 4, NULL),
	(39, 32, 8, 4, NULL),
	(208, 45, 27, 2, NULL),
	(209, 45, 28, 2, NULL),
	(210, 45, 29, 2, NULL),
	(211, 45, 30, 2, NULL),
	(212, 45, 31, 2, NULL),
	(213, 45, 32, 2, NULL),
	(214, 45, 33, 2, NULL),
	(215, 45, 34, 2, NULL),
	(216, 45, 35, 2, NULL),
	(217, 45, 36, 2, NULL),
	(218, 45, 37, 2, NULL),
	(219, 45, 38, 2, NULL),
	(220, 45, 39, 2, NULL),
	(221, 45, 40, 2, NULL),
	(222, 46, 27, 3, NULL),
	(223, 46, 28, 4, NULL),
	(224, 46, 29, 3, NULL),
	(225, 46, 30, 4, NULL),
	(226, 46, 31, 22, NULL),
	(227, 46, 32, 4, NULL),
	(228, 46, 33, 3, NULL),
	(229, 46, 34, 4, NULL),
	(230, 46, 35, 2, NULL),
	(231, 46, 36, 4, NULL),
	(232, 46, 37, 2, NULL),
	(233, 46, 38, 23, NULL),
	(234, 46, 39, 3, NULL),
	(235, 46, 40, 3, NULL),
	(236, 47, 27, 3, NULL),
	(237, 47, 28, 4, NULL),
	(238, 47, 29, 3, NULL),
	(239, 47, 30, 4, NULL),
	(240, 47, 31, 22, NULL),
	(241, 47, 32, 4, NULL),
	(242, 47, 33, 3, NULL),
	(243, 47, 34, 4, NULL),
	(244, 47, 35, 2, NULL),
	(245, 47, 36, 4, NULL),
	(246, 47, 37, 2, NULL),
	(247, 47, 38, 23, NULL),
	(248, 47, 39, 3, NULL),
	(249, 47, 40, 3, NULL),
	(250, 48, 27, 2, NULL),
	(251, 48, 28, 3, NULL),
	(252, 48, 29, 4, NULL),
	(253, 48, 30, 3, NULL),
	(254, 48, 31, 4, NULL),
	(255, 48, 32, 3, NULL),
	(256, 48, 33, 2, NULL),
	(257, 48, 34, 4, NULL),
	(258, 48, 35, 22, NULL),
	(259, 48, 36, 3, NULL),
	(260, 48, 37, 2, NULL),
	(261, 48, 38, 2, NULL),
	(262, 48, 39, 3, NULL),
	(263, 48, 40, 2, NULL);
/*!40000 ALTER TABLE `user_test_answer` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
