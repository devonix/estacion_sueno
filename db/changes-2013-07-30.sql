ALTER TABLE `user_reaction_test`
	ADD COLUMN `invalid_count` INT(10) UNSIGNED NOT NULL AFTER `alert_level`,
	ADD COLUMN `missed_count` INT(10) UNSIGNED NOT NULL AFTER `invalid_count`,
	ADD COLUMN `crash_count` INT(10) UNSIGNED NOT NULL AFTER `missed_count`;
	
ALTER TABLE `user_reaction_test`
	CHANGE COLUMN `time_now` `time_now` DECIMAL(10,4) UNSIGNED NOT NULL AFTER `awake_hours`;	