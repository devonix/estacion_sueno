ALTER TABLE `estacion_sueno`.`user_test`   
  CHANGE `score` `score` DECIMAL(10,4) NOT NULL;
  
CREATE TABLE `estacion_sueno`.`user_test_reaction`(  
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_test_id` INT UNSIGNED NOT NULL,
  `sleep_hours` INT UNSIGNED NOT NULL,
  `awake_hours` INT UNSIGNED NOT NULL,
  `time_now` TIME NOT NULL,
  `alert_level` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_test_id`) REFERENCES `estacion_sueno`.`user_test`(`id`)
);

ALTER TABLE `estacion_sueno`.`user_test_measurements`   
  CHANGE `user_test_id` `user_test_reaction_id` INT(10) UNSIGNED NOT NULL,
  DROP FOREIGN KEY `user_test_measurements_ibfk_1`,
  ADD FOREIGN KEY (`user_test_reaction_id`) REFERENCES `estacion_sueno`.`user_test_reaction`(`id`);

RENAME TABLE `estacion_sueno`.`user_test_measurements` TO `estacion_sueno`.`user_test_reaction_measurements`;

RENAME TABLE `estacion_sueno`.`user_test_reaction` TO `estacion_sueno`.`user_reaction_test`;

ALTER TABLE `estacion_sueno`.`user_test_reaction_measurements`   
  CHANGE `user_test_reaction_id` `user_reaction_test_id` INT(10) UNSIGNED NOT NULL,
  DROP FOREIGN KEY `user_test_reaction_measurements_ibfk_2`,
  ADD FOREIGN KEY (`user_reaction_test_id`) REFERENCES `estacion_sueno`.`user_reaction_test`(`id`);

RENAME TABLE `estacion_sueno`.`user_test_reaction_measurements` TO `estacion_sueno`.`user_reaction_test_measurement`;

