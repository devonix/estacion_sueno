ALTER TABLE `profile` CHANGE `working_days_sleep_hours` `working_days_sleep_hours` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `working_days_sleep_hours_desired` `working_days_sleep_hours_desired` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `weekend_sleep_hours` `weekend_sleep_hours` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `weekend_sleep_hours_desired` `weekend_sleep_hours_desired` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `working_hours` `working_hours` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `exercise_hours` `exercise_hours` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `recreation_hours` `recreation_hours` TIME DEFAULT '00:00:00' NOT NULL; 
ALTER TABLE `profile` CHANGE `travel_hours` `travel_hours` TIME DEFAULT '00:00:00' NOT NULL; 
UPDATE `profile_field` SET `field_type` = 'TIME' WHERE `id` IN ('20', '21', '23', '24', '26', '27', '28', '29');