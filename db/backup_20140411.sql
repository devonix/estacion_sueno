/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.6.13-log : Database - estacion_sueno2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `authassignment` */

DROP TABLE IF EXISTS `authassignment`;

CREATE TABLE `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `authassignment` */

insert  into `authassignment`(`itemname`,`userid`,`bizrule`,`data`) values ('Admin','1',NULL,'N;');

/*Table structure for table `authitem` */

DROP TABLE IF EXISTS `authitem`;

CREATE TABLE `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `authitem` */

insert  into `authitem`(`name`,`type`,`description`,`bizrule`,`data`) values ('Admin',2,NULL,NULL,'N;'),('Authenticated',2,NULL,NULL,'N;'),('Guest',2,NULL,NULL,'N;'),('Site.Index',0,NULL,NULL,'N;'),('Table.*',1,NULL,NULL,'N;'),('Table.Admin',0,NULL,NULL,'N;'),('Table.Create',0,NULL,NULL,'N;'),('Table.Delete',0,NULL,NULL,'N;'),('Table.Index',0,NULL,NULL,'N;'),('Table.Update',0,NULL,NULL,'N;'),('Table.View',0,NULL,NULL,'N;'),('User.Activation.*',1,NULL,NULL,'N;'),('User.Activation.Activation',0,NULL,NULL,'N;'),('User.Default.*',1,NULL,NULL,'N;'),('User.Default.Index',0,NULL,NULL,'N;'),('User.Login.*',1,NULL,NULL,'N;'),('User.Login.Login',0,NULL,NULL,'N;'),('User.Logout.*',1,NULL,NULL,'N;'),('User.Logout.Logout',0,NULL,NULL,'N;'),('User.Profile.*',1,NULL,NULL,'N;'),('User.Profile.Changepassword',0,NULL,NULL,'N;'),('User.Profile.Edit',0,NULL,NULL,'N;'),('User.Profile.Profile',0,NULL,NULL,'N;'),('User.Recovery.*',1,NULL,NULL,'N;'),('User.Recovery.Recovery',0,NULL,NULL,'N;'),('User.User.*',1,NULL,NULL,'N;'),('User.User.Index',0,NULL,NULL,'N;'),('User.User.View',0,NULL,NULL,'N;');

/*Table structure for table `authitemchild` */

DROP TABLE IF EXISTS `authitemchild`;

CREATE TABLE `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `authitemchild` */

insert  into `authitemchild`(`parent`,`child`) values ('Authenticated','Site.Index'),('Authenticated','Table.*'),('Authenticated','Table.Create'),('Authenticated','Table.Index'),('Authenticated','Table.Update'),('Authenticated','Table.View'),('Authenticated','User.Activation.*'),('Authenticated','User.Activation.Activation'),('Authenticated','User.Default.*'),('Authenticated','User.Default.Index'),('Authenticated','User.Login.Login'),('Authenticated','User.Logout.*'),('Authenticated','User.Logout.Logout'),('Authenticated','User.Profile.*'),('Authenticated','User.Profile.Changepassword'),('Authenticated','User.Profile.Edit'),('Authenticated','User.Profile.Profile'),('Authenticated','User.Recovery.Recovery');

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `company` */

insert  into `company`(`id`,`name`) values (1,'Empresa 1'),(2,'Empresa 2');

/*Table structure for table `occupation` */

DROP TABLE IF EXISTS `occupation`;

CREATE TABLE `occupation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `occupation` */

insert  into `occupation`(`id`,`name`) values (1,'Empresario'),(2,'Profesional Autónomo'),(3,'Comerciante'),(4,'Dirección de Empresas'),(5,'Gerencia'),(6,'Jefatura'),(7,'Supervisión'),(8,'Empleado'),(9,'En búsqueda laboral'),(10,'Otros');

/*Table structure for table `occupation_area` */

DROP TABLE IF EXISTS `occupation_area`;

CREATE TABLE `occupation_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `occupation_area` */

insert  into `occupation_area`(`id`,`name`) values (1,'Administración pública'),(2,'Agricultura, ganadería, pesca'),(3,'Alimentos y bebidas'),(4,'Banca y servicios financieros'),(5,'Comunicación, marketing y publicidad'),(6,'Construcción y mercado inmobiliario'),(7,'Consultoría'),(8,'Consumo masivo'),(9,'Cosmética y limpieza'),(10,'Defensa y seguridad'),(11,'Electrónica y electrodomésticos'),(12,'Energía, electricidad, gas y petróleo'),(13,'Enseñanza'),(14,'Estudios jurídicos'),(15,'Eventos y exposiciones'),(16,'Indumentaria'),(17,'Industria automotriz'),(18,'Laboratorios'),(19,'Medios de comunicación'),(20,'Mercado de capitales'),(21,'Minería'),(22,'ONG, servicios comunitarios y sociales'),(23,'Química y petroquímica'),(24,'Retail'),(25,'Seguros'),(26,'Servicios a empresas'),(27,'Servicios de salud'),(28,'Siderurgia y metalurgia'),(29,'Tabacaleras'),(30,'Tecnología'),(31,'Telecomunicaciones'),(32,'Transporte'),(33,'Turismo, hotelería, restaurantes'),(34,'Universidades y educación'),(35,'Otras actividades');

/*Table structure for table `profile` */

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender` int(10) NOT NULL DEFAULT '0',
  `height` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `age` int(10) NOT NULL DEFAULT '0',
  `marital_status` int(10) NOT NULL DEFAULT '0',
  `activity_level` int(10) NOT NULL DEFAULT '0',
  `working_days_sleep_hours` time NOT NULL DEFAULT '00:00:00',
  `working_days_sleep_hours_desired` time NOT NULL DEFAULT '00:00:00',
  `working_days_sleep_quality` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weekend_sleep_hours` time NOT NULL DEFAULT '00:00:00',
  `weekend_sleep_hours_desired` time NOT NULL DEFAULT '00:00:00',
  `weekend_sleep_quality` int(10) NOT NULL DEFAULT '0',
  `working_hours` time NOT NULL DEFAULT '00:00:00',
  `exercise_hours` time NOT NULL DEFAULT '00:00:00',
  `recreation_hours` time NOT NULL DEFAULT '00:00:00',
  `travel_hours` time NOT NULL DEFAULT '00:00:00',
  `company_id` int(10) NOT NULL DEFAULT '0',
  `occupation_id` int(10) NOT NULL DEFAULT '0',
  `occupation_area_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `profile` */

insert  into `profile`(`user_id`,`name`,`gender`,`height`,`weight`,`age`,`marital_status`,`activity_level`,`working_days_sleep_hours`,`working_days_sleep_hours_desired`,`working_days_sleep_quality`,`weekend_sleep_hours`,`weekend_sleep_hours_desired`,`weekend_sleep_quality`,`working_hours`,`exercise_hours`,`recreation_hours`,`travel_hours`,`company_id`,`occupation_id`,`occupation_area_id`) values (1,'Administrador',0,1.80,60.00,0,0,0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,0,0),(6,'Demo',1,1.80,60.00,0,1,1,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,0,0),(7,'Maxi',1,1.50,52.00,42,3,3,'00:00:01','00:00:02',1.00,'00:00:08','00:00:22',1,'00:00:00','00:00:00','00:00:24','00:00:00',0,0,0),(8,'Maxi2',1,1.80,60.00,0,1,1,'00:00:01','00:00:01',1.00,'00:00:01','00:00:01',1,'00:00:01','00:00:01','00:00:01','00:00:01',0,0,0),(9,'Nombre',1,1.80,60.00,0,4,3,'00:00:03','00:00:03',2.00,'00:00:02','00:00:02',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,0,0),(10,'test3',1,1.68,68.00,61,3,2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,0,0),(15,'asdf',2,0.00,0.00,0,3,2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',1,0,0),(16,'test4',1,0.00,0.00,0,2,2,'00:00:03','00:00:00',3.00,'00:00:02','00:00:02',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,2,14),(17,'asdf',2,0.00,0.00,0,3,3,'00:00:08','00:00:09',3.00,'00:00:07','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,1,16),(18,'asdf4',2,0.00,0.00,0,3,3,'00:00:01','00:00:01',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,3,16),(19,'asdf',2,0.00,0.00,0,4,3,'03:45:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,4,15),(20,'asdf',2,0.00,0.00,0,4,3,'04:45:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,4,15),(21,'asdf2',2,0.00,0.00,0,3,3,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00',0,7,15);

/*Table structure for table `profile_field` */

DROP TABLE IF EXISTS `profile_field`;

CREATE TABLE `profile_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*Data for the table `profile_field` */

insert  into `profile_field`(`id`,`varname`,`title`,`field_type`,`field_size`,`field_size_min`,`required`,`match`,`range`,`error_message`,`other_validator`,`default`,`widget`,`widgetparams`,`position`,`visible`) values (5,'name','Name','VARCHAR',255,0,1,'','','','','','','',0,1),(12,'gender','Sexo','INTEGER',10,0,1,'','1==Masculino;2==Femenino','','','0','','',10,1),(13,'height','Altura','DECIMAL',10,0,1,'','','','','0','','',20,1),(14,'weight','Peso','DECIMAL',10,0,1,'','','','','0','','',30,1),(15,'age','Edad','INTEGER',10,0,1,'','','','','0','','',40,1),(16,'marital_status','Estado civil','INTEGER',10,0,1,'','1==Soltero;2==Casado;3==Divorciado;4==Viudo','','','0','','',50,1),(19,'activity_level','Nivel de actividad','INTEGER',10,0,1,'','1==Sedentario;2==Activo;3==Muy activo','','','0','','',75,1),(20,'working_days_sleep_hours','Horas de sueño habituales en días laborales','TIME',10,0,1,'','','','','0','','',80,1),(21,'working_days_sleep_hours_desired','Horas de sueño deseadas en días laborales','TIME',10,0,1,'','','','','0','','',80,1),(22,'working_days_sleep_quality','Calidad de sueño en días laborales','DECIMAL',10,0,1,'','','','','0','','',80,1),(23,'weekend_sleep_hours','Horas de sueño habituales en fines de semana','TIME',10,0,1,'','','','','0','','',110,1),(24,'weekend_sleep_hours_desired','Horas de sueño deseadas por noche en fines de semana','TIME',10,0,1,'','','','','0','','',120,1),(25,'weekend_sleep_quality','Calidad de sueño en fines de semana','INTEGER',10,0,1,'','','','','0','','',130,1),(26,'working_hours','Horas de trabajo','TIME',10,0,1,'','','','','0','','',140,1),(27,'exercise_hours','Horas de actividad física','TIME',10,0,1,'','','','','0','','',150,1),(28,'recreation_hours','Horas de actividad recreativa','TIME',10,0,1,'','','','','0','','',160,1),(29,'travel_hours','Horas de viaje','TIME',10,0,1,'','','','','0','','',170,1),(30,'company_id','Empresa','INTEGER',10,0,2,'','','','','0','UWrelBelongsTo','{\"modelName\":\"Company\",\"optionName\":\"name\",\"emptyField\":\"Ninguno\",\"relationName\":\"company\"}',0,0),(31,'occupation_id','Profesión','INTEGER',10,0,1,'','','','','0','UWrelBelongsTo','{\"modelName\":\"Occupation\",\"optionName\":\"name\",\"relationName\":\"occupation\"}',61,1),(32,'occupation_area_id','Área de actividad','INTEGER',10,0,1,'','','','','0','UWrelBelongsTo','{\"modelName\":\"OccupationArea\",\"optionName\":\"name\",\"relationName\":\"occupation_area\"}',71,1);

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL,
  `number` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  `question_type` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

/*Data for the table `question` */

insert  into `question`(`id`,`test_id`,`number`,`text`,`question_type`) values (1,3,1,'Sentado leyendo',1),(2,3,2,'Viendo televisión',1),(3,3,3,'Sentado, inactivo en un lugar público',1),(4,3,4,'Como pasajero en un coche una hora seguida',1),(5,3,5,'Descansando recostado por la tarde cuando las circunsatancias lo permiten',1),(6,3,6,'Sentado charlando con alguien',1),(7,3,7,'Sentado tranquilo, después de un almuerzo sin alcohol',1),(8,3,8,'En un coche, al pararse unos minutos en el tráfico',1),(9,1,1,'en su casa, ¿a qué hora se acuesta normalmente por la noche?',2),(10,1,2,'¿Cúanto tiempo demora en quedarse dormido en promedio?',2),(11,1,3,'¿A qué hora se levanta habitualmente por la mañana?',2),(12,1,4,'¿Cuántas horas duerme habitualmente cada noche?',2),(13,1,5,'No poder quedarse dormido en la primera media hora',3),(14,1,6,'Despertarse durante la noche o de madrugada',3),(15,1,7,'Tener que levantarse para ir al baño',3),(16,1,8,'No poder respirar bien',3),(17,1,9,'Toser o roncar ruidosamente',3),(18,1,10,'Sentir  frio',3),(19,1,11,'Sentir calor',3),(20,1,12,'Tener sueños feos o pesadillas',3),(21,1,13,'Tener dolores',3),(22,1,14,'Otras razones',3),(23,1,15,'Durante el mes pasado... ¿cuántas veces ha tomado medicinas (recetadas por el medico o por su cuenta) para dormir?',3),(24,1,16,'Durante el mes pasado... ¿cuántas veces ha tenido problemas para permanecer despierto mientras conducía, comía, trabajaba, estudiaba o desarrollaba alguna otra actividad social?',3),(25,1,17,'Durante el último mes, ¿qué tanto problema le ha traído a usted su estado de ánimo para realizar actividades como conducir, comer, trabajar, estudiar o alguna otra actividad social?',4),(26,1,18,'Durante el último mes, ¿cómo calificaría en conjunto la calidad de su sueño?',5),(27,2,1,'Ronquido fuerte',6),(28,2,2,'¿Siente las piernas inquietas, como si saltaran, o que se sacuden? (En cualquier momento del día)?',6),(29,2,3,'Dificultad para conciliar el sueño',6),(30,2,4,'Despertares frecuentes',6),(31,2,5,'¿Tiene dificultad para respirar? ¿Respiración entrecortada? ¿Respiración ruidosa? (En cualquier momento del día)',6),(32,2,6,'¿Se siente adormecido en el trabajo? (mientras conduce)',6),(33,2,7,'¿Con frecuencia da vueltas o se sacude en la cama?',6),(34,2,8,'¿En algún momento siente que se queda sin aire? (en cualquier momento del día)',6),(35,2,9,'¿Tiene excesiva somnolencia (le da mucho sueño) durante el día?',6),(36,2,10,'¿Tiene dolores de cabeza por la mañana?',6),(37,2,11,'¿Se sintió adormecido al conducir? (Fuera de su trabajo)',6),(38,2,12,'Se siente paralizado, incapaz de moverse por períodos cortos al dormirse o al despertar',6),(39,2,13,'¿Tiene sueños vívidos al quedarse dormido o en el momento de despertarse?',6),(40,2,14,'Ronquido (Cualquiera; fuerte o débil)',6);

/*Table structure for table `question_type` */

DROP TABLE IF EXISTS `question_type`;

CREATE TABLE `question_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `question_type` */

insert  into `question_type`(`id`,`name`) values (1,'Epworth'),(2,'Pittsburgh - 1'),(3,'Pittsburgh - 2'),(4,'Pittsburgh - 3'),(5,'Pittsburgh - 4'),(6,'MAP');

/*Table structure for table `question_type_option` */

DROP TABLE IF EXISTS `question_type_option`;

CREATE TABLE `question_type_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_type_id` int(10) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question_type_id` (`question_type_id`),
  CONSTRAINT `question_type_option_ibfk_1` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `question_type_option` */

insert  into `question_type_option`(`id`,`question_type_id`,`value`,`text`) values (1,1,0,'Ninguna vez en el último mes'),(2,1,1,'Menos de una vez por semana'),(3,1,2,'Una o dos veces a la semana'),(4,1,3,'Tres o más veces a la semana'),(5,2,0,'Escriba la hora habitual en la que se acuesta hs/min'),(6,3,0,'Ninguna vez en el último mes'),(7,3,1,'Menos de una vez a la semana'),(8,3,2,'Una o dos veces a la semana'),(9,3,3,'Tres o más veces a la semana'),(10,4,0,'Ningún problema'),(11,4,1,'Poco problema'),(12,4,2,'Moderado problema'),(13,4,3,'Mucho problema'),(14,5,0,'Muy buena'),(15,5,1,'Bastante buena'),(16,5,2,'Bastante mala'),(17,5,3,'Muy mala'),(18,6,0,'Nunca'),(19,6,1,'Casi nunca'),(20,6,2,'A veces'),(21,6,3,'Con frecuencia'),(22,6,4,'Siempre'),(23,6,5,'No sabe');

/*Table structure for table `rights` */

DROP TABLE IF EXISTS `rights`;

CREATE TABLE `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`),
  CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rights` */

/*Table structure for table `tbl_migration` */

DROP TABLE IF EXISTS `tbl_migration`;

CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_migration` */

insert  into `tbl_migration`(`version`,`apply_time`) values ('m000000_000000_base',1357784830),('m110805_153437_installYiiUser',1357784843),('m110810_162301_userTimestampFix',1357784843);

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_reaction_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `test` */

insert  into `test`(`id`,`name`,`is_reaction_test`) values (1,'Calidad del sueño',0),(2,'Predicción de apnea',0),(3,'Escala del sueño',0),(4,'Tiempo de reacción',1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`email`,`activkey`,`superuser`,`status`,`create_at`,`lastvisit_at`) values (1,'admin','21232f297a57a5a743894a0e4a801fc3','info@pixelmachine.com.ar','ebe83c88cff438a889d3a833d6b2d9e5',1,1,'2013-01-09 23:27:23','2014-02-27 17:27:03'),(6,'demo','fe01ce2a7fbac8fafaed7c982a04e229','demo@pixelmachine.com.ar','2957aefeba7be834458f490d84577314',1,1,'2013-05-27 16:41:45','2013-09-10 22:48:57'),(7,'maxi','d6af6111b120b3b6d0edad7d1d3fc692','mnusspaumer@pixelmachine.com.ar','49dbaa1666aea3feaf46980b0e4b3222',0,1,'2013-06-12 15:40:40','2013-09-10 22:50:07'),(8,'maxi2','c92d56303b128ae61e1c0af1af1cd7fd','maxi@pixelmachine.com.ar','57d3f10c36016da5be411c980bb875ca',0,1,'2013-06-12 15:51:28','2013-06-17 19:48:21'),(9,'usuario_test','81dc9bdb52d04dc20036dbd8313ed055','test@devonix.net','85fa6d944d2bf8733e4245b50ccd83cf',0,1,'2013-07-04 17:42:45','0000-00-00 00:00:00'),(10,'test3','81dc9bdb52d04dc20036dbd8313ed055','test3@devonix.net','b994bebd65eb276ff116b95414ca8f4a',0,1,'2013-07-04 17:55:12','2014-02-19 22:11:34'),(15,'asdf','912ec803b2ce49e4a541068d495ab570','asdf@asdf.com','fd1a3d823ed968e79309a183c5b911aa',0,1,'2013-09-11 19:47:58','0000-00-00 00:00:00'),(16,'test4','86985e105f79b95d6bc918fb45ec7727','test4@server.com','d8eaca1b44155e78f39d83d25d1ad606',0,1,'2013-12-12 15:59:06','0000-00-00 00:00:00'),(17,'asdf3','e2fc714c4727ee9395f324cd2e7f331f','info@devonix.net','edaa6875aa35e906203bddfa8bd28723',0,1,'2013-12-12 17:11:30','0000-00-00 00:00:00'),(18,'adf4','81dc9bdb52d04dc20036dbd8313ed055','asdf4@server.com','d7bf92423dfb226ceffad830551a2b96',0,1,'2013-12-12 17:17:06','0000-00-00 00:00:00'),(19,'asdf6','83e5f15a32d2074a5e08b8cc5e965cb9','asdf6@server.com','592a8cacc5683552eb1f9b75e3ee0aba',0,1,'2013-12-30 15:11:58','0000-00-00 00:00:00'),(20,'asdf7','83e5f15a32d2074a5e08b8cc5e965cb9','asdf7@server.com','00175395e2f22e01b6667d8653602805',0,1,'2013-12-30 15:14:11','0000-00-00 00:00:00'),(21,'asdf12','af5b3d17aa1e2ff2a0f83142d692d701','asdf12@server.com','6407d7e05812ef7095e786364f7cdc7a',0,1,'2014-01-03 15:17:51','0000-00-00 00:00:00');

/*Table structure for table `user_reaction_test` */

DROP TABLE IF EXISTS `user_reaction_test`;

CREATE TABLE `user_reaction_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_test_id` int(10) unsigned NOT NULL,
  `sleep_hours` int(10) unsigned NOT NULL,
  `awake_hours` int(10) unsigned NOT NULL,
  `time_now` decimal(10,4) unsigned NOT NULL,
  `alert_level` int(10) unsigned NOT NULL,
  `invalid_count` int(10) unsigned NOT NULL,
  `missed_count` int(10) unsigned NOT NULL,
  `crash_count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_test_id` (`user_test_id`),
  CONSTRAINT `user_reaction_test_ibfk_1` FOREIGN KEY (`user_test_id`) REFERENCES `user_test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `user_reaction_test` */

insert  into `user_reaction_test`(`id`,`user_test_id`,`sleep_hours`,`awake_hours`,`time_now`,`alert_level`,`invalid_count`,`missed_count`,`crash_count`) values (4,58,1,1,10.0000,1,0,0,0),(5,59,1,1,10.0000,1,0,0,0),(6,60,1,1,10.0000,1,0,0,0),(7,61,1,1,10.0000,1,0,0,0),(8,62,1,1,10.0000,5,0,0,0),(9,63,8,3,10.5000,8,0,0,0),(10,64,8,6,10.7500,8,0,0,0),(11,65,1,1,10.0167,5,0,1,3),(12,69,1,1,10.0167,5,14,0,4),(13,70,1,1,10.0167,5,4,4,1),(14,72,1,1,1.0167,5,0,1,2),(15,74,7,4,15.8667,5,8,0,0),(16,75,0,0,16.8833,5,3,28,1),(17,76,0,0,17.0833,5,1,28,2),(18,77,0,0,17.4000,5,4,28,1),(19,78,0,0,17.6500,5,0,26,2),(20,79,0,0,15.6667,5,2,20,6),(21,80,0,0,15.8500,5,0,24,2),(22,81,0,0,15.9500,5,0,22,6),(23,82,0,0,16.0667,5,0,22,4),(24,83,0,0,18.9833,5,2,5,1),(25,84,0,0,19.1167,5,2,26,3);

/*Table structure for table `user_reaction_test_measurement` */

DROP TABLE IF EXISTS `user_reaction_test_measurement`;

CREATE TABLE `user_reaction_test_measurement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_reaction_test_id` int(10) unsigned NOT NULL,
  `value` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_reaction_test_id` (`user_reaction_test_id`),
  CONSTRAINT `user_reaction_test_measurement_ibfk_3` FOREIGN KEY (`user_reaction_test_id`) REFERENCES `user_reaction_test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;

/*Data for the table `user_reaction_test_measurement` */

insert  into `user_reaction_test_measurement`(`id`,`user_reaction_test_id`,`value`) values (1,4,453.00),(2,4,533.00),(3,5,453.00),(4,5,533.00),(5,6,453.00),(6,6,533.00),(7,7,453.00),(8,7,533.00),(9,8,98.00),(10,8,475.00),(11,9,430.00),(12,9,413.00),(13,10,454.00),(14,10,498.00),(15,10,501.00),(16,10,476.00),(17,10,619.00),(18,10,395.00),(19,10,674.00),(20,10,423.00),(21,10,497.00),(22,10,456.00),(23,10,592.00),(24,10,493.00),(25,10,551.00),(26,10,482.00),(27,10,678.00),(28,10,635.00),(29,10,690.00),(30,10,504.00),(31,10,1521.00),(32,10,-1.00),(33,10,513.00),(34,10,585.00),(35,10,856.00),(36,10,459.00),(37,10,578.00),(38,10,472.00),(39,10,496.00),(40,10,525.00),(41,10,525.00),(42,10,546.00),(43,11,464.00),(44,11,892.00),(45,11,916.00),(46,11,463.00),(47,11,1024.00),(48,12,2204.00),(49,12,606.00),(50,12,806.00),(51,12,555.00),(52,12,313.00),(53,12,339.00),(54,13,4323.00),(55,13,372.00),(56,14,373.00),(57,14,404.00),(58,14,403.00),(59,14,378.00),(60,14,447.00),(61,14,458.00),(62,14,474.00),(63,14,492.00),(64,14,523.00),(65,14,479.00),(66,14,467.00),(67,14,468.00),(68,14,398.00),(69,14,431.00),(70,14,475.00),(71,14,435.00),(72,14,370.00),(73,14,359.00),(74,14,382.00),(75,14,423.00),(76,14,371.00),(77,14,420.00),(78,14,568.00),(79,14,390.00),(80,14,388.00),(81,14,416.00),(82,14,383.00),(83,14,370.00),(84,14,374.00),(85,15,324.00),(86,15,336.00),(87,15,292.00),(88,15,294.00),(89,15,276.00),(90,15,375.00),(91,15,338.00),(92,15,353.00),(93,15,296.00),(94,15,292.00),(95,15,335.00),(96,15,337.00),(97,15,310.00),(98,15,329.00),(99,15,325.00),(100,15,320.00),(101,15,149.00),(102,15,309.00),(103,15,317.00),(104,15,311.00),(105,15,321.00),(106,15,336.00),(107,15,335.00),(108,15,302.00),(109,15,334.00),(110,15,318.00),(111,15,398.00),(112,15,326.00),(113,15,325.00),(114,15,334.00),(115,16,3338.00),(116,16,390.00),(117,17,1061.00),(118,17,2152.00),(119,18,140.00),(120,18,3744.00),(121,19,2542.00),(122,19,4836.00),(123,19,343.00),(124,19,328.00),(125,20,436.00),(126,20,546.00),(127,20,515.00),(128,20,374.00),(129,20,530.00),(130,20,343.00),(131,20,546.00),(132,20,546.00),(133,20,531.00),(134,20,453.00),(135,21,359.00),(136,21,484.00),(137,21,422.00),(138,21,874.00),(139,21,562.00),(140,21,375.00),(141,22,390.00),(142,22,406.00),(143,22,515.00),(144,22,500.00),(145,22,733.00),(146,22,530.00),(147,22,717.00),(148,22,1669.00),(149,23,639.00),(150,23,437.00),(151,23,546.00),(152,23,452.00),(153,23,577.00),(154,23,343.00),(155,23,655.00),(156,23,499.00),(157,24,47.00),(158,24,327.00),(159,24,281.00),(160,24,312.00),(161,24,328.00),(162,24,328.00),(163,24,312.00),(164,24,359.00),(165,24,343.00),(166,24,328.00),(167,24,328.00),(168,24,390.00),(169,24,453.00),(170,24,327.00),(171,24,327.00),(172,24,375.00),(173,24,375.00),(174,24,375.00),(175,24,359.00),(176,24,359.00),(177,24,422.00),(178,24,655.00),(179,24,343.00),(180,24,421.00),(181,24,375.00),(182,25,374.00),(183,25,1311.00),(184,25,702.00),(185,25,624.00);

/*Table structure for table `user_test` */

DROP TABLE IF EXISTS `user_test`;

CREATE TABLE `user_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `test_id` int(10) unsigned NOT NULL,
  `date_taken` datetime NOT NULL,
  `score` decimal(10,4) NOT NULL,
  `gender` int(10) NOT NULL DEFAULT '0',
  `height` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `age` int(10) NOT NULL DEFAULT '0',
  `marital_status` int(10) NOT NULL DEFAULT '0',
  `occupation` varchar(255) NOT NULL DEFAULT '',
  `occupation_area` varchar(255) NOT NULL DEFAULT '',
  `activity_level` int(10) NOT NULL DEFAULT '0',
  `working_days_sleep_hours` time NOT NULL DEFAULT '00:00:00',
  `working_days_sleep_hours_desired` time NOT NULL DEFAULT '00:00:00',
  `working_days_sleep_quality` decimal(10,2) NOT NULL DEFAULT '0.00',
  `weekend_sleep_hours` time NOT NULL DEFAULT '00:00:00',
  `weekend_sleep_hours_desired` time NOT NULL DEFAULT '00:00:00',
  `weekend_sleep_quality` int(10) NOT NULL DEFAULT '0',
  `working_hours` time NOT NULL DEFAULT '00:00:00',
  `exercise_hours` time NOT NULL DEFAULT '00:00:00',
  `recreation_hours` time NOT NULL DEFAULT '00:00:00',
  `travel_hours` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `user_test_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_test_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `test` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

/*Data for the table `user_test` */

insert  into `user_test`(`id`,`user_id`,`test_id`,`date_taken`,`score`,`gender`,`height`,`weight`,`age`,`marital_status`,`occupation`,`occupation_area`,`activity_level`,`working_days_sleep_hours`,`working_days_sleep_hours_desired`,`working_days_sleep_quality`,`weekend_sleep_hours`,`weekend_sleep_hours_desired`,`weekend_sleep_quality`,`working_hours`,`exercise_hours`,`recreation_hours`,`travel_hours`) values (30,10,3,'2013-07-11 17:33:47',0.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(31,10,3,'2013-07-12 14:50:48',0.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(32,10,3,'2013-07-16 12:20:18',24.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(45,10,2,'2013-07-23 16:05:11',0.1602,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(46,10,2,'2013-07-23 16:08:03',0.3995,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(47,10,2,'2013-07-23 16:10:23',0.3995,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(48,6,2,'2013-07-24 19:49:05',0.0349,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(58,10,4,'2013-07-29 18:02:35',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(59,10,4,'2013-07-29 18:02:59',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(60,10,4,'2013-07-29 18:03:25',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(61,10,4,'2013-07-29 18:03:40',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(62,10,4,'2013-07-29 18:28:14',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(63,10,4,'2013-07-30 13:09:24',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(64,10,4,'2013-07-30 13:13:27',10.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(65,10,4,'2013-07-30 15:17:06',751.8000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(69,10,4,'2013-07-30 15:32:19',803.8333,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(70,10,4,'2013-07-30 16:12:38',2347.5000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(71,10,1,'2013-08-15 13:15:28',9.0000,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(72,6,4,'2013-08-23 12:13:44',424.7931,0,0.00,0.00,0,0,'','',0,'00:00:00','00:00:00',0.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(73,10,1,'2013-08-23 18:25:10',16.0000,1,1.68,68.00,61,3,'profesion','area',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(74,10,4,'2013-10-07 15:55:41',318.2333,1,1.68,68.00,61,3,'profesion','area',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(75,10,4,'2013-12-13 16:56:09',1864.0000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(76,10,4,'2013-12-13 17:08:11',1606.5000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(77,10,4,'2013-12-13 17:28:00',1942.0000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(78,10,4,'2013-12-13 17:42:54',2012.2500,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(79,10,4,'2013-12-16 15:47:44',482.0000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(80,10,4,'2013-12-16 15:56:14',512.6667,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(81,10,4,'2013-12-16 16:00:19',682.5000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(82,10,4,'2013-12-16 16:07:37',518.5000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(83,10,4,'2013-12-17 19:02:22',353.9600,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(84,10,4,'2013-12-17 19:10:50',752.7500,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00'),(85,10,1,'2014-02-27 13:26:39',11.0000,1,1.68,68.00,61,3,'','',2,'00:00:00','00:00:00',3.00,'00:00:00','00:00:00',0,'00:00:00','00:00:00','00:00:00','00:00:00');

/*Table structure for table `user_test_answer` */

DROP TABLE IF EXISTS `user_test_answer`;

CREATE TABLE `user_test_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_test_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `question_type_option_id` int(10) unsigned DEFAULT NULL,
  `value` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_test_id` (`user_test_id`),
  KEY `question_id` (`question_id`),
  KEY `question_type_option_id` (`question_type_option_id`),
  CONSTRAINT `user_test_answer_ibfk_1` FOREIGN KEY (`user_test_id`) REFERENCES `user_test` (`id`),
  CONSTRAINT `user_test_answer_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `user_test_answer_ibfk_3` FOREIGN KEY (`question_type_option_id`) REFERENCES `question_type_option` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=latin1;

/*Data for the table `user_test_answer` */

insert  into `user_test_answer`(`id`,`user_test_id`,`question_id`,`question_type_option_id`,`value`) values (16,30,1,1,NULL),(17,30,2,1,NULL),(18,30,3,1,NULL),(19,30,4,1,NULL),(20,30,5,2,NULL),(21,30,6,2,NULL),(22,30,7,2,NULL),(23,30,8,2,NULL),(24,31,1,3,NULL),(25,31,2,3,NULL),(26,31,3,3,NULL),(27,31,4,4,NULL),(28,31,5,4,NULL),(29,31,6,3,NULL),(30,31,7,4,NULL),(31,31,8,3,NULL),(32,32,1,4,NULL),(33,32,2,4,NULL),(34,32,3,4,NULL),(35,32,4,4,NULL),(36,32,5,4,NULL),(37,32,6,4,NULL),(38,32,7,4,NULL),(39,32,8,4,NULL),(208,45,27,2,NULL),(209,45,28,2,NULL),(210,45,29,2,NULL),(211,45,30,2,NULL),(212,45,31,2,NULL),(213,45,32,2,NULL),(214,45,33,2,NULL),(215,45,34,2,NULL),(216,45,35,2,NULL),(217,45,36,2,NULL),(218,45,37,2,NULL),(219,45,38,2,NULL),(220,45,39,2,NULL),(221,45,40,2,NULL),(222,46,27,3,NULL),(223,46,28,4,NULL),(224,46,29,3,NULL),(225,46,30,4,NULL),(226,46,31,22,NULL),(227,46,32,4,NULL),(228,46,33,3,NULL),(229,46,34,4,NULL),(230,46,35,2,NULL),(231,46,36,4,NULL),(232,46,37,2,NULL),(233,46,38,23,NULL),(234,46,39,3,NULL),(235,46,40,3,NULL),(236,47,27,3,NULL),(237,47,28,4,NULL),(238,47,29,3,NULL),(239,47,30,4,NULL),(240,47,31,22,NULL),(241,47,32,4,NULL),(242,47,33,3,NULL),(243,47,34,4,NULL),(244,47,35,2,NULL),(245,47,36,4,NULL),(246,47,37,2,NULL),(247,47,38,23,NULL),(248,47,39,3,NULL),(249,47,40,3,NULL),(250,48,27,2,NULL),(251,48,28,3,NULL),(252,48,29,4,NULL),(253,48,30,3,NULL),(254,48,31,4,NULL),(255,48,32,3,NULL),(256,48,33,2,NULL),(257,48,34,4,NULL),(258,48,35,22,NULL),(259,48,36,3,NULL),(260,48,37,2,NULL),(261,48,38,2,NULL),(262,48,39,3,NULL),(263,48,40,2,NULL),(264,71,9,NULL,2.50),(265,71,10,NULL,0.17),(266,71,11,NULL,11.00),(267,71,12,NULL,8.00),(268,71,13,1,NULL),(269,71,14,2,NULL),(270,71,15,2,NULL),(271,71,16,3,NULL),(272,71,17,2,NULL),(273,71,18,3,NULL),(274,71,19,2,NULL),(275,71,20,2,NULL),(276,71,21,3,NULL),(277,71,22,3,NULL),(278,71,23,4,NULL),(279,71,24,4,NULL),(280,71,25,2,NULL),(281,71,26,3,NULL),(282,73,9,NULL,5.00),(283,73,10,NULL,5.00),(284,73,11,NULL,5.00),(285,73,12,NULL,4.00),(286,73,13,2,NULL),(287,73,14,2,NULL),(288,73,15,3,NULL),(289,73,16,2,NULL),(290,73,17,3,NULL),(291,73,18,2,NULL),(292,73,19,2,NULL),(293,73,20,2,NULL),(294,73,21,2,NULL),(295,73,22,2,NULL),(296,73,23,2,NULL),(297,73,24,4,NULL),(298,73,25,3,NULL),(299,73,26,3,NULL),(300,85,9,NULL,0.25),(301,85,10,NULL,0.25),(302,85,11,NULL,0.25),(303,85,12,NULL,0.25),(304,85,13,2,NULL),(305,85,14,2,NULL),(306,85,15,2,NULL),(307,85,16,2,NULL),(308,85,17,2,NULL),(309,85,18,2,NULL),(310,85,19,2,NULL),(311,85,20,2,NULL),(312,85,21,2,NULL),(313,85,22,2,NULL),(314,85,23,2,NULL),(315,85,24,2,NULL),(316,85,25,2,NULL),(317,85,26,2,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
