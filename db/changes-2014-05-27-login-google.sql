ALTER TABLE `profile`
ADD COLUMN `provider` VARCHAR(255) NOT NULL DEFAULT '',
ADD COLUMN `provider_id` VARCHAR(255) NOT NULL DEFAULT '';

INSERT INTO `profile_field` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES('33','provider','Proveedor','VARCHAR','255','0','0','','','','','','','','33','1');
INSERT INTO `profile_field` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES('35','provider_id','Id del proveedor','VARCHAR','255','0','0','','','','','','','','0','1');
